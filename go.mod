module github.com/TruthHun/BookStack

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/TruthHun/converter v0.0.0-20180618131031-80268d67d7a8
	github.com/TruthHun/gotil v0.0.0-20180131052058-ae4443562325 // indirect
	github.com/TruthHun/html2article v0.0.0-20180202140721-67d6ff09647b
	github.com/TruthHun/html2md v0.0.0-20190507142218-8352cc68f88e
	github.com/adamzy/cedar-go v0.0.0-20170805034717-80a9c64b256d // indirect
	github.com/alexcesaro/mail v0.0.0-20141015155039-29068ce49a17
	github.com/aliyun/aliyun-oss-go-sdk v2.0.1+incompatible
	github.com/astaxie/beego v1.12.0
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/boombuler/barcode v1.0.0
	github.com/disintegration/imaging v1.6.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/huichen/sego v0.0.0-20180617034105-3f3c8a8cfacc
	github.com/issue9/assert v1.3.3 // indirect
	github.com/kardianos/service v1.0.0
	github.com/lc3091/gotil v0.0.0-20190814102414-994be503d152
	github.com/lifei6671/gocaptcha v0.0.0-20190301083731-c467a25bc100
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/unknwon/com v0.0.0-20190804042917-757f69c95f3e
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	golang.org/x/tools v0.0.0-20190813214729-9dba7caff850
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/ldap.v2 v2.5.1
)
